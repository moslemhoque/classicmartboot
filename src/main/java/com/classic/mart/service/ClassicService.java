package com.classic.mart.service;

import java.util.List;

import com.classic.mart.model.MenuDTO;

public interface ClassicService {

	List<MenuDTO> getMenu();

}
