package com.classic.mart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.classic.mart.dao.ClassicDao;
import com.classic.mart.model.MenuDTO;

@Component("classicService")
public class ClassicServiceImp implements ClassicService{

	@Autowired
	private ClassicDao classicDao;
	
	@Override
	public List<MenuDTO> getMenu() {
		return classicDao.getMenu();
	}

}
