package com.classic.mart.dao;

import java.util.List;

import com.classic.mart.model.MenuDTO;

public interface ClassicDao {

	List<MenuDTO> getMenu();

}
