package com.classic.mart.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.classic.mart.configarator.ClassicMartLogger;
import com.classic.mart.model.MenuDTO;

@Component("classicDao")
public class ClassicDaoImp implements ClassicDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final String GET_SQL_QUERY = "SELECT ID,MENU_NAME,PARENT_ID,SEQ_ID,"
												+ "USER_TYPE,MENU_CATEGORY FROM CLASSIC.MENU";
	/**
	 * getMenu
	 * @return menuDTO
	 */
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<MenuDTO> getMenu() {
		ClassicMartLogger.debugIn("getMenu",this.getClass());

		List<MenuDTO> menuList = jdbcTemplate.query(GET_SQL_QUERY, new BeanPropertyRowMapper(MenuDTO.class));
		
		ClassicMartLogger.debugOut("getMenu",this.getClass());
		return menuList;
	}

}
