/*
 * Classic Mart 2018-2019 copy right
 * Created by Kazi
 */
package com.classic.mart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classic.mart.model.MenuDTO;
import com.classic.mart.service.ClassicService;


@RestController
@RequestMapping("/classic")
public class classicController {
	
	@Autowired
	private ClassicService classicService;
	
	/**
	 * getMenu
	 * @return
	 */
	@GetMapping("/getMenu")
	public List<MenuDTO> getMenu(){
		return classicService.getMenu();
	}

}
