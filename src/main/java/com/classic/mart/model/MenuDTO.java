package com.classic.mart.model;

public class MenuDTO {
	
	private String id;
	private String menuName;
	private String menuCategory;
	private String userType;
	private String parentId;
	private String seqId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuCategory() {
		return menuCategory;
	}
	public void setMenuCategory(String menuCategory) {
		this.menuCategory = menuCategory;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getSeqId() {
		return seqId;
	}
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	
	
	@Override
	public String toString() {
		return "MenuDTO [id=" + id + ", menuName=" + menuName + ", menuCategory=" + menuCategory + ", userType="
				+ userType + ", parentId=" + parentId + ", seqId=" + seqId + "]";
	}
	
}
