package com.classic.mart.configarator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.classic.mart")
public class ClassicMartApplication {
	public static void main(String[] args) {
		SpringApplication.run(ClassicMartApplication.class, args);
	}
}
