package com.classic.mart.configarator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  @author Kazi
 *
 */
public class ClassicMartLogger {
	public static final String LOGGER_NAME = "classicLogger";
	public static Logger log = LoggerFactory.getLogger(LOGGER_NAME);
	
	/**
	 * debugIn
	 * @param method
	 * @param clazz
	 * @return
	 */
	public static boolean debugIn(String method, Class<?> clazz){
		if(log.isDebugEnabled()){
		log.debug(message("Entering Method"+"."+ clazz + "." + method));	
		}
		return true;
	}
	
	/**
	 * debugOut
	 * @param method
	 * @param clazz
	 * @return
	 */
	public static boolean debugOut(String method, Class<?> clazz){
		if(log.isDebugEnabled()){
		log.debug(message("Leaving Method"+"."+ clazz + "." + method));	
		}
		return true;
	}
	
	/**
	 * debug
	 * @param message
	 * @return
	 */
	public static boolean debug(Object message){
		if(log.isDebugEnabled()){
			log.debug(message(message));
		}
		return true;
		
	}
	
	/**
	 * error
	 * @param message
	 * @return
	 */
	public static boolean error(Object message){
		if(log.isDebugEnabled()){
			log.debug(message(message));
		}
		return true;
		
	}

	/**
	 * message
	 * @param message
	 * @return
	 */
	private static String message(Object message) {
		Thread thread = Thread.currentThread();
		StackTraceElement[] stack = thread.getStackTrace();
		StackTraceElement up = stack[3];
		return up.getMethodName() + "-" +message;
	}

}
