(function () {

	var app = angular.module('myApp');
	app.factory('imageService', ['$http', '$q', function ($http, $q ) {

		return {
			
			sendImageFiles : sendImageFiles
		};
		
		
		function sendImageFiles(requestData) {

			var deffered = $q.defer();
			var data = JSON.stringify(requestData)
			$http.post("/spring3-mvc-maven-xml-hello-world/sendImageFile",requestData)
			.success(function(data, status){
				deffered.resolve(data, status);
			
			})
			.error(function(data, status){
			});
			return deffered.promise;
		}
		
		
		
		
		
	}])}());

		